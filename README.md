Sketchness-Proxy
---

### Presequisites

_PM2_
```bash
$ npm install -g pm2
```


### Installation

```bash
$ npm install --production
```

Create a file targets.json with the sintax:

```js
{
	"host": {...},
	...
}
```
"default" is the target used if no target is defined

The object for each host follows the structure defined here
[!node-http-proxy](https://github.com/nodejitsu/node-http-proxy#options)

### Start server

```bash
$ npm start
```

### Stop server

```bash
$ npm stop
```

### Restart server

```bash
$ npm restart
```

### Get server status

```bash
$ npm test
```