/*jslint node: true */
/** 
* Developed By Carlo Bernaschina (GitLab/GitHub - B3rn475) 
* www.bernaschina.com 
* 
* Distributed under the MIT Licence 
*/ 
"use strict";

var httpProxy = require('http-proxy'),
    http = require('http'),
    targets = require('./targets');

var proxy = httpProxy.createProxyServer();

proxy.on('error', function (e) {
    console.error(e);
});

function getTarget(host) {
    var target = targets[host];
    if (typeof target === 'object') { return target; }
    if (typeof target === 'string') { return getTarget(target); }
}

http.createServer(function (req, res) {
    var target = getTarget(req.headers.host) || getTarget('default');
    if (target) {
        proxy.web(req, res, target);
    } else {
        console.log('Unknown Host :' + req.headers.host);
        res.end();
    }
}).on('upgrade', function (req, socket, head) {
    var target = getTarget(req.headers.host) || getTarget('default');
    if (target) {
        if (target.ws) {
            proxy.ws(req, socket, head, target);
        } else {
            console.log('Not supported websocket: ' + req.headers.host);
            res.end();
        }
    } else {
        console.log('Unknown Host: ' + req.headers.host);
        res.end();
    }
}).listen(process.env.port || 3000);
